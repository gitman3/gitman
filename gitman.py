# region Imports

import os
import shutil
import sys
import git
import re

from pathlib import Path

# endregion

version = '1.00'

# region Os Paths

if os.name == 'posix':
	queryPath = str(Path.home()) + '/.local/share/gitman/query'
	try:
		query = open(queryPath)
		query.close()
	except:
		if not os.path.exists((str(Path.home()) + '/.local/share/gitman/')):
			os.mkdir(str(Path.home()) + '/.local/share/gitman/')
		query = open(queryPath, 'w')
		query.close()
elif os.name == 'nt':
	print('Windows version is in development')
	sys.exit()
else:
	print('Unsupported operating system')
	sys.exit()
# endregion

# region Args

try:
	if len(sys.argv) > 1:
		# region Help

		if sys.argv[1] == '-H' or sys.argv[1] == '--help':
			if len(sys.argv) > 2:
				print("Help does not take any arguments")
			else:
				print('''Welcome to gitman.
				To add repositories to your query type "gitman -A (or) --add (path of the repositories with a space between them)"
				To remove repositories from query type "gitman -R (or) --remove (names of the repositories with a space between them)"
				To remove repositories permanently type "gitman -Rs (or) --remove-source (names of the repositories with a space between them)"
				To pull repositories from query type "gitman -S (or) --sync (name of the repositories with space a between them)"
				To pull all of your repositories type "gitman -Sa (or) --sync-all"
				To clone a repository type "gitman -C (or) --clone (source address of the repository then the path you want to put it with a space between them)"
				To clone a repository to a directory named after the git repository type "gitman -Cs (or) --clone-subdir (source address of the repository then the path you want to put it with a space between them)"
				To view your query type "gitman -Q (or) --query"
				To view your path query type "gitman -Qp (or) --query-path"
				To view the version of gitman type "gitman -V (or) --version"
				To view this message type "gitman -H (or) --help"''')
		# endregion
		# region Query

		elif sys.argv[1] == '-Q' or sys.argv[1] == '--query':
			if len(sys.argv) > 2:
				print('Query does not take any arguments')
			else:
				with open(queryPath) as query:
					for line in query:
						if line.rsplit('/', 1)[-1].rstrip() != '':
							print(line.rsplit('/', 1)[-1].rstrip())
						else:
							print(line.rsplit('/', 2)[-2].rstrip())
		# endregion
		# region Query-Path

		elif sys.argv[1] == '-Qp' or sys.argv[1] == '--query-path':
			if len(sys.argv) > 2:
				print('Query-Path does not take any arguments')
			else:
				with open(queryPath) as query:
					for line in query:
						print(line.rstrip())
		# endregion
		# region Version

		elif sys.argv[1] == '-V' or sys.argv[1] == '--version':
			if len(sys.argv) > 2:
				print('Version does not take any arguments')
			else:
				print(version)
		# endregion
		# region Add

		elif sys.argv[1] == '-A' or sys.argv[1] == '--add':
			if len(sys.argv) <= 2:
				print('Add requires arguments')
			else:
				query = open(queryPath, 'a')
				repoCounter = 2
				while repoCounter < len(sys.argv):
					if sys.argv[repoCounter].endswith('/\n'):
						sys.argv[repoCounter] = sys.argv[repoCounter].rstrip('/\n') + '\n'
					if sys.argv[repoCounter].startswith('~') == True:
						repoPath = sys.argv[repoCounter].replace('~', str(os.path.expanduser('~')))
					else:
						repoPath = Path(sys.argv[repoCounter])
					repoAbsPath = os.path.abspath(repoPath)
					if os.name == 'posix':
						repoAbsPath = repoAbsPath.replace(os.path.expanduser('~'), '~')
					if os.path.exists(os.path.expanduser(repoAbsPath)):
						query.write(repoAbsPath + '\n')
						print('The Path "' + repoAbsPath + '" is added to the query')
					else:
						print('The path "' + repoAbsPath + '" does not exist')
					repoCounter = repoCounter + 1
				query.close()
		# endregion
		# region Remove

		elif sys.argv[1] == '-R' or sys.argv[1] == '--remove':
			if len(sys.argv) <= 2:
				print('Remove requires arguments')
			else:
				i = 2
				while i < len(sys.argv):
					query = open(queryPath)
					repoCounter = 0
					repoList = []
					repoSplittedList = []
					for selectedLine in query:
						if selectedLine.endswith(sys.argv[i] + '\n') or selectedLine.endswith(sys.argv[i] + '/' + '\n'):
							if selectedLine.startswith('~') == True:
								selectedLine = selectedLine.replace('~', str(os.path.expanduser('~')))
							if selectedLine.endswith('/\n') == True:
								selectedLine = selectedLine.rstrip('/\n') + '\n'
							repoCounter = repoCounter + 1
							repoList += [selectedLine]
							repoSplittedList += [selectedLine.rsplit('/', 1)[-1].rstrip()]
					if repoCounter >= 2:
						print('There are ' + str(repoCounter) + ' repositories that has the name ' + sys.argv[i] + ' which one would you like to remove')
						while True:
							lineNumber = 1
							while lineNumber - 1 < repoCounter:
								print(str(lineNumber) + ': ' + repoList[lineNumber - 1].rstrip())
								lineNumber = lineNumber + 1
							try:
								repoToRemove = int(input())
							except:
								print('Invalid selection, please try again')
								continue
							if repoToRemove > len(repoList) or repoToRemove <= 0:
								print('Invalid selection, please try again')
								continue
							with open(queryPath) as query:
								lines = query.readlines()
							with open(queryPath, 'w') as query:
								alreadyDeletedALine = False
								for line in lines:
									if line != repoList[repoToRemove - 1] or line != str(repoList[repoToRemove - 1]).replace(os.path.expanduser('~'), '~') or alreadyDeletedALine == True:
										query.write(line)
									else:
										alreadyDeletedALine = True
							print('The repo "' + sys.argv[i] + '" is removed from the query')
							break
					elif repoCounter == 1:
						with open(queryPath) as query:
							lines = query.readlines()
						with open(queryPath, 'w') as query:
							for line in lines:
								if line != repoList[0] or line != str(repoList[0]).replace(os.path.expanduser('~'), '~'):
									query.write(line)
						print('The repo "' + sys.argv[i] + '" is removed from the query')
					else:
						print('Could not find the repo "' + sys.argv[i] + '" please ensure the repo exists')
					i = i + 1
		# endregion
		# region Remove-Source

		elif sys.argv[1] == '-Rs' or sys.argv[1] == '--remove-source':
			if len(sys.argv) <= 2:
				print('Remove-Source requires arguments')
			else:
				i = 2
				while i < len(sys.argv):
					query = open(queryPath)
					repoCounter = 0
					repoList = []
					repoSplittedList = []
					for selectedLine in query:
						if selectedLine.endswith(sys.argv[i] + '\n') or selectedLine.endswith(sys.argv[i] + '/' + '\n'):
							if selectedLine.startswith('~') == True:
								selectedLine = selectedLine.replace('~', str(os.path.expanduser('~')))
							if selectedLine.endswith('/\n') == True:
								selectedLine = selectedLine.rstrip('/\n') + '\n'
							repoCounter = repoCounter + 1
							repoList += [selectedLine]
							repoSplittedList += [selectedLine.rsplit('/', 1)[-1].rstrip()]
					if len(repoList) == 0:
						repoPath = sys.argv[i]
						if os.name == 'posix':
							if sys.argv[i].startswith('/') or sys.argv[i].startswith('~') or sys.argv[i].startswith('.'):
								repoPath = os.path.abspath(str(Path(sys.argv[i])).replace('~', os.path.expanduser('~')))
					else:
						repoPath = repoList[i-2]
					if os.path.exists(repoPath) == True:
						if sys.argv[i] in repoList or sys.argv[i] in repoSplittedList:
							if repoCounter >= 2:
								print('There are ' + str(repoCounter) + ' repositories that has the name or path ' + sys.argv[i] + ' which one would you like to remove')
								while True:
									lineNumber = 1
									while lineNumber - 1 < repoCounter:
										print(str(lineNumber) + ': ' + repoList[lineNumber - 1].rstrip())
										lineNumber = lineNumber + 1
									try:
										repoToRemove = int(input())
									except:
										print('Invalid selection, please try again')
										continue
									if repoToRemove > len(repoList) or repoToRemove <= 0:
										print('Invalid selection, please try again')
										continue
									with open(queryPath) as query:
										lines = query.readlines()
									with open(queryPath, 'w') as query:
										alreadyDeletedALine = False
										for line in lines:
											if line != repoList[repoToRemove - 1] or line != str(repoList[repoToRemove - 1]).replace(os.path.expanduser('~'), '~') or alreadyDeletedALine == True:
												query.write(line)
											else:
												alreadyDeletedALine = True
									shutil.rmtree(repoList[repoToRemove - 1].rstrip())
									print('The repo "' + sys.argv[i] + '" is removed')
									break
							elif repoCounter == 1:
								with open(queryPath) as query:
									lines = query.readlines()
								with open(queryPath, 'w') as query:
									for line in lines:
										if line != repoList[0] or line != str(repoList[0]).replace(os.path.expanduser('~'), '~'):
											query.write(line)
								shutil.rmtree(repoList[0].rstrip())
								print('The repo "' + sys.argv[i] + '" is removed')
							else:
								print('Could not find the repo "' + sys.argv[i] + '" please ensure the repo exists')
						else:
							print('The repo "' + sys.argv[i] + '" is not in the query, do you want to remove the source anyways [y/N]')
							operationComplete = False
							while operationComplete == False:
								answer = input()
								if answer == 'y' or answer == 'Y':
									if os.path.exists(sys.argv[i]):
										shutil.rmtree(sys.argv[i])
										print('The folder "' + sys.argv[i] + '" is removed')
									else:
										print('Path not found')
									operationComplete = True
								elif answer == 'n' or answer == 'N' or answer == '':
									operationComplete = True
								else:
									print('Invalid input please try again')
					else:
						if sys.argv[i] in repoList or sys.argv[i] in repoSplittedList:
							print('The directory "' + str(repoList[repoCounter - 1]).replace(os.path.expanduser('~'), '~').rstrip() + '" does not exist do you want to remove the query entry [Y/n]')
							operationComplete = False
							while operationComplete == False:
								answer = input()
								if answer == 'y' or answer == 'Y' or answer == '':
									if repoCounter >= 2:
										print(
											'There are ' + str(repoCounter) + ' repositories that has the name ' + sys.argv[i] + ' which one would you like to remove')
										while True:
											lineNumber = 1
											while lineNumber - 1 < repoCounter:
												print(str(lineNumber) + ': ' + repoList[lineNumber - 1].rstrip())
												lineNumber = lineNumber + 1
											try:
												repoToRemove = int(input())
											except:
												print('Invalid selection, please try again')
												continue
											if repoToRemove > len(repoList) or repoToRemove <= 0:
												print('Invalid selection, please try again')
												continue
											with open(queryPath) as query:
												lines = query.readlines()
											with open(queryPath, 'w') as query:
												alreadyDeletedALine = False
												for line in lines:
													if line.startswith(os.path.expanduser('~')) == True:
														line = line.replace(os.path.expanduser('~'), '~')
													if line != str(repoList[repoToRemove - 1]).replace(os.path.expanduser('~'), '~') or alreadyDeletedALine == True:
														query.write(line)
													else:
														alreadyDeletedALine = True
											print('The repo "' + sys.argv[i] + '" is removed from the query')
											break
									elif repoCounter == 1:
										with open(queryPath) as query:
											lines = query.readlines()
										with open(queryPath, 'w') as query:
											for line in lines:
												if line.startswith(os.path.expanduser('~')) == True:
													line = line.replace(os.path.expanduser('~'), '~')
												if line != str(repoList[0]).replace(os.path.expanduser('~'), '~'):
													query.write(line)
										print('The repo "' + sys.argv[i] + '" is removed from the query')
									operationComplete = True
								elif answer == 'n' or answer == 'N':
									operationComplete = True
								else:
									print('Invalid input please try again')
						else:
							print('The repository "' + sys.argv[i] + '" exists neither on the filesystem or query, operation canceled for that entry')
					i = i + 1
		# endregion
		# region Clone

			if len(sys.argv) <= 3:
				print('Clone requires two arguments')
			elif len(sys.argv) >= 5:
				print('Clone only takes two arguments')
			else:
				with open(queryPath) as query:
					lines = query.readlines()
				lineExistsInQuery = False
				for line in lines:
					if sys.argv[3] == line or sys.argv[3].replace(os.path.expanduser('~'), '~') == line:
						lineExistsInQuery = True
				if lineExistsInQuery == True:
					print('Line already exists in query do you want to clone anyways [y/N]')
					operationComplete = False
					while operationComplete == False:
						answer = input()
						if answer == 'y' or answer == 'Y':
							urlChecker = re.compile(
								r'^(?:http|ftp)s?://'  # http:// or https://
								r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'  # domain...
								r'localhost|'  # localhost...
								r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
								r'(?::\d+)?'  # optional port
								r'(?:/?|[/?]\S+)$', re.IGNORECASE)

							if re.match(urlChecker, sys.argv[2]) is None:
								print('Invalid url')
							else:
								if os.path.exists(sys.argv[3]) == False:
									print('Path does not exist do you want to create a path [y/N]')
									operationComplete = False
									while operationComplete == False:
										answer = input()
										if answer == 'y' or answer == 'Y':
											try:
												if sys.argv[3].startswith('~') == True:
													clonePath = sys.argv[3].replace('~', str(os.path.expanduser('~')))
												else:
													clonePath = Path(sys.argv[3])
												cloneAbsPath = os.path.abspath(clonePath)
												os.mkdir(cloneAbsPath)
												print('The directory "' + cloneAbsPath + '" is created')
												try:
													git.Repo.clone_from(sys.argv[2], cloneAbsPath)
													if cloneAbsPath.startswith('/') == True and os.name == 'posix':
														cloneAbsPath = cloneAbsPath.replace(os.path.expanduser('~'), '~')
													query.write(cloneAbsPath + '\n')
													print('Repo successfully cloned')
												except:
													print('Git clone error')
											except:
												print(
													'File creation error, please make sure the path you entered is possible for your operating system')
											operationComplete = True
										elif answer == 'n' or answer == 'N' or answer == '':
											print('Operation canceled')
											operationComplete = True
										else:
											print('Invalid input please try again')
								else:
									if sys.argv[3].startswith('~') == True:
										clonePath = sys.argv[3].replace('~', str(os.path.expanduser('~')))
									else:
										clonePath = Path(sys.argv[3])
									cloneAbsPath = os.path.abspath(clonePath) + '/' + sys.argv[2].rsplit('/', 2)[-2]
									try:
										git.Repo.clone_from(sys.argv[2], cloneAbsPath)
										if cloneAbsPath.startswith('/') == True and os.name == 'posix':
											cloneAbsPath = cloneAbsPath.replace(os.path.expanduser('~'), '~')
										query.write(cloneAbsPath + '\n')
										print('Repo successfully cloned and added to query')
									except:
										print('Git clone error')
							operationComplete = True
						elif answer == 'n' or answer == 'N' or answer == '':
							operationComplete = True
						else:
							print('Invalid input please try again')
				else:
					repoPath = sys.argv[2]
					if os.name == 'posix':
						if sys.argv[2].startswith('/') or sys.argv[2].startswith('~') or sys.argv[2].startswith('.'):
							repoPath = os.path.abspath(str(Path(sys.argv[2])).replace('~', os.path.expanduser('~')))
					if os.path.exists(repoPath):
						print('Path already exists do you want to clone anyways or just add entry to query')
						userInput = 0
						while userInput < 1 or userInput > 3:
							print('1: Clone anyways')
							print('2: Just add entry to query')
							print('3: Cancel operation')
							try:
								userInput = int(input())
							except:
								print('Invalid selection, please try again')
								continue
							if userInput == 1:
								query = open(queryPath, 'a')
								urlChecker = re.compile(
									r'^(?:http|ftp)s?://'  # http:// or https://
									r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'  # domain...
									r'localhost|'  # localhost...
									r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
									r'(?::\d+)?'  # optional port
									r'(?:/?|[/?]\S+)$', re.IGNORECASE)

								if re.match(urlChecker, sys.argv[2]) is None:
									print('Invalid url')
								else:
									if os.path.exists(sys.argv[3]) == False:
										print('Path does not exist do you want to create a path [y/N]')
										operationComplete = False
										while operationComplete == False:
											answer = input()
											if answer == 'y' or answer == 'Y':
												try:
													if sys.argv[3].startswith('~') == True:
														clonePath = sys.argv[3].replace('~', str(os.path.expanduser('~')))
													else:
														clonePath = Path(sys.argv[3])
													cloneAbsPath = os.path.abspath(clonePath)
													os.mkdir(cloneAbsPath)
													print('The directory "' + cloneAbsPath + '" is created')
													try:
														git.Repo.clone_from(sys.argv[2], cloneAbsPath)
														if cloneAbsPath.startswith('/') == True and os.name == 'posix':
															cloneAbsPath = cloneAbsPath.replace(os.path.expanduser('~'), '~')
														query.write(cloneAbsPath + '\n')
														print('Repo successfully cloned and added to query')
													except:
														print('Git clone error')
												except:
													print(
														'File creation error, please make sure the path you entered is possible for your operating system')
												operationComplete = True
											elif answer == 'n' or answer == 'N' or answer == '':
												print('Operation canceled')
												operationComplete = True
											else:
												print('Invalid input please try again')
									else:
										if sys.argv[3].startswith('~') == True:
											clonePath = sys.argv[3].replace('~', str(os.path.expanduser('~')))
										else:
											clonePath = Path(sys.argv[3])
										cloneAbsPath = os.path.abspath(clonePath)
										try:
											git.Repo.clone_from(sys.argv[2], cloneAbsPath)
											if cloneAbsPath.startswith('/') == True and os.name == 'posix':
												cloneAbsPath = cloneAbsPath.replace(os.path.expanduser('~'), '~')
											query.write(cloneAbsPath + '\n')
											print('Repo successfully cloned and added to query')
										except:
											print('Git clone error')
								query.close()
							elif userInput == 2:
								with open(queryPath, 'a') as query:
									if sys.argv[3].startswith('~') == True:
										repoPath = sys.argv[3].replace('~', str(os.path.expanduser('~')))
									else:
										repoPath = Path(sys.argv[3])
									repoAbsPath = os.path.abspath(repoPath)
									if os.name == 'posix':
										repoAbsPath = repoAbsPath.replace(os.path.expanduser('~'), '~')
									query.write(repoAbsPath)
							elif userInput == 3:
								print('Operation canceled')
							else:
								print('Invalid selection, please try again')
								continue
		# endregion
		# region Clone-Subdir

		elif sys.argv[1] == '-Cs' or sys.argv[1] == '--clone-subdir':
			if len(sys.argv) <= 3:
				print('Clone-Subdir requires two arguments')
			elif len(sys.argv) >= 5:
				print('Clone-Subdir only takes two arguments')
			else:
				with open(queryPath) as query:
					lines = query.readlines()
				lineExistsInQuery = False
				for line in lines:
					if sys.argv[3] == line or sys.argv[3].replace(os.path.expanduser('~'), '~') == line:
						lineExistsInQuery = True
				if lineExistsInQuery == True:
					print('Line already exists in query do you want to clone anyways [y/N]')
					operationComplete = False
					while operationComplete == False:
						answer = input()
						if answer == 'y' or answer == 'Y':
							urlChecker = re.compile(
								r'^(?:http|ftp)s?://'  # http:// or https://
								r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'  # domain...
								r'localhost|'  # localhost...
								r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
								r'(?::\d+)?'  # optional port
								r'(?:/?|[/?]\S+)$', re.IGNORECASE)

							if re.match(urlChecker, sys.argv[2]) is None:
								print('Invalid url')
							else:
								if os.path.exists(sys.argv[3]) == False:
									print('Path does not exist do you want to create a path [y/N]')
									operationComplete = False
									while operationComplete == False:
										answer = input()
										if answer == 'y' or answer == 'Y':
											try:
												if sys.argv[3].startswith('~') == True:
													clonePath = sys.argv[3].replace('~', str(os.path.expanduser('~')))
												else:
													clonePath = Path(sys.argv[3])
												cloneAbsPath = os.path.abspath(clonePath)
												os.mkdir(cloneAbsPath)
												if sys.argv[2].endswith('.git') == True or sys.argv[2].endswith('.git/'):
													cloneRepoPath = os.path.abspath(clonePath) + '/' + \
																	sys.argv[2].rsplit('.', 1)[-1]
												elif sys.argv[2].endswith('/') == True:
													cloneRepoPath = os.path.abspath(clonePath) + '/' + \
																	sys.argv[2].rsplit('/', 2)[-2]
												else:
													cloneRepoPath = os.path.abspath(clonePath) + '/' + \
																	sys.argv[2].rsplit('/', 1)[-1]
												os.mkdir(cloneRepoPath)
												print('The directory "' + cloneRepoPath + '" is created')
												try:
													git.Repo.clone_from(sys.argv[2], cloneRepoPath)
													if cloneRepoPath.startswith('/') == True and os.name == 'posix':
														cloneRepoPath = cloneRepoPath.replace(os.path.expanduser('~'), '~')
													print('Repo successfully cloned')
												except:
													print('Git clone error')
											except:
												print(
													'File creation error, please make sure the path you entered is possible for your operating system')
											operationComplete = True
										elif answer == 'n' or answer == 'N' or answer == '':
											print('Operation canceled')
											operationComplete = True
										else:
											print('Invalid input please try again')
								else:
									if sys.argv[3].startswith('~') == True:
										clonePath = sys.argv[3].replace('~', str(os.path.expanduser('~')))
									else:
										clonePath = Path(sys.argv[3])
									cloneAbsPath = os.path.abspath(clonePath) + '/' + sys.argv[2].rsplit('/', 2)[-2]
									try:
										git.Repo.clone_from(sys.argv[2], cloneAbsPath)
										if cloneAbsPath.startswith('/') == True and os.name == 'posix':
											cloneAbsPath = cloneAbsPath.replace(os.path.expanduser('~'), '~')
										query.write(cloneAbsPath + '\n')
										print('Repo successfully cloned and added to query')
									except:
										print('Git clone error')
							operationComplete = True
						elif answer == 'n' or answer == 'N' or answer == '':
							operationComplete = True
						else:
							print('Invalid input please try again')
				else:
					repoPath = sys.argv[2]
					if os.name == 'posix':
						if sys.argv[2].startswith('/') or sys.argv[2].startswith('~') or sys.argv[2].startswith('.'):
							repoPath = os.path.abspath(str(Path(sys.argv[2])).replace('~', os.path.expanduser('~')))
					if os.path.exists(repoPath):
						print('Path already exists do you want to clone anyways or just add entry to query')
						userInput = 0
						while userInput < 1 or userInput > 3:
							print('1: Clone anyways')
							print('2: Just add entry to query')
							print('3: Cancel operation')
							try:
								userInput = int(input())
							except:
								print('Invalid selection, please try again')
								continue
							if userInput == 1:
								query = open(queryPath, 'a')
								urlChecker = re.compile(
									r'^(?:http|ftp)s?://'  # http:// or https://
									r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'  # domain...
									r'localhost|'  # localhost...
									r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
									r'(?::\d+)?'  # optional port
									r'(?:/?|[/?]\S+)$', re.IGNORECASE)

								if re.match(urlChecker, sys.argv[2]) is None:
									print('Invalid url')
								else:
									if os.path.exists(sys.argv[3]) == False:
										print('Path does not exist do you want to create a path [y/N]')
										operationComplete = False
										while operationComplete == False:
											answer = input()
											if answer == 'y' or answer == 'Y':
												try:
													if sys.argv[3].startswith('~') == True:
														clonePath = sys.argv[3].replace('~', str(os.path.expanduser('~')))
													else:
														clonePath = Path(sys.argv[3])
													cloneAbsPath = os.path.abspath(clonePath)
													os.mkdir(cloneAbsPath)
													if sys.argv[2].endswith('.git') == True or sys.argv[2].endswith('.git/'):
														cloneRepoPath = os.path.abspath(clonePath) + '/' + sys.argv[2].rsplit('.', 1)[-1]
													elif sys.argv[2].endswith('/') == True:
														cloneRepoPath = os.path.abspath(clonePath) + '/' + sys.argv[2].rsplit('/', 2)[-2]
													else:
														cloneRepoPath = os.path.abspath(clonePath) + '/' + sys.argv[2].rsplit('/', 1)[-1]
													os.mkdir(cloneRepoPath)
													print('The directory "' + cloneRepoPath + '" is created')
													try:
														git.Repo.clone_from(sys.argv[2], cloneRepoPath)
														if cloneRepoPath.startswith('/') == True and os.name == 'posix':
															cloneRepoPath = cloneRepoPath.replace(os.path.expanduser('~'), '~')
														query.write(cloneRepoPath + '\n')
														print('Repo successfully cloned and added to query')
													except:
														print('Git clone error')
												except:
													print('File creation error, please make sure the path you entered is possible for your operating system')
												operationComplete = True
											elif answer == 'n' or answer == 'N' or answer == '':
												print('Operation canceled')
												operationComplete = True
											else:
												print('Invalid input please try again')
									else:
										if sys.argv[3].startswith('~') == True:
											clonePath = sys.argv[3].replace('~', str(os.path.expanduser('~')))
										else:
											clonePath = Path(sys.argv[3])
										cloneAbsPath = os.path.abspath(clonePath) + '/' + sys.argv[2].rsplit('/', 2)[-2]
										try:
											git.Repo.clone_from(sys.argv[2], cloneAbsPath)
											if cloneAbsPath.startswith('/') == True and os.name == 'posix':
												cloneAbsPath = cloneAbsPath.replace(os.path.expanduser('~'), '~')
											query.write(cloneAbsPath + '\n')
											print('Repo successfully cloned and added to query')
										except:
											print('Git clone error')
								query.close()
							elif userInput == 2:
								with open(queryPath, 'a') as query:
									if sys.argv[3].startswith('~') == True:
										repoPath = sys.argv[3].replace('~', str(os.path.expanduser('~')))
									else:
										repoPath = Path(sys.argv[3])
									repoAbsPath = os.path.abspath(repoPath)
									if os.name == 'posix':
										repoAbsPath = repoAbsPath.replace(os.path.expanduser('~'), '~')
									query.write(repoAbsPath + '/' + sys.argv[2].rsplit('.', 1)[-1])
							elif userInput == 3:
								print('Operation canceled')
							else:
								print('Invalid selection, please try again')
								continue
		# endregion
		# region Sync

		elif sys.argv[1] == '-S' or sys.argv[1] == '--sync':
			if len(sys.argv) <= 2:
				print('Sync requires arguments')
			else:
				i = 2
				while i < len(sys.argv):
					query = open(queryPath)
					repoCounter = 0
					repoList = []
					repoSplittedList = []
					for selectedLine in query:
						if selectedLine.endswith(sys.argv[i] + '\n') or selectedLine.endswith(sys.argv[i] + '/' + '\n'):
							if selectedLine.startswith('~') == True:
								selectedLine = selectedLine.replace('~', str(os.path.expanduser('~')))
							if selectedLine.endswith('/\n') == True:
								selectedLine = selectedLine.rstrip('/\n') + '\n'
							repoCounter = repoCounter + 1
							repoList += [selectedLine]
							repoSplittedList += [selectedLine.rsplit('/', 1)[-1].rstrip()]
					if sys.argv[i] in repoList or sys.argv[i] in repoSplittedList:
						try:
							repo = git.Repo(str(repoList[repoCounter - 1]).rstrip())
							origin = repo.remote()
							try:
								with open(str(repoList[repoCounter - 1]).rstrip() + '/.git/FETCH_HEAD') as head:
									headBeforePull = head.readline()
								origin.pull()
								with open(str(repoList[repoCounter - 1]).rstrip() + '/.git/FETCH_HEAD') as head:
									headAfterPull = head.readline()
								if headBeforePull == headAfterPull:
									print('The repository "' + repoSplittedList[repoCounter - 1] + '" is already up to date')
								else:
									print('The repository "' + repoSplittedList[repoCounter - 1] + '" is updated')
							except FileNotFoundError:
								print('The head file for the repository"' + repoSplittedList[repoCounter - 1] + '" could not found, please ensure the path is actually an git directory')
						except:
							print('The files for the repository "' + repoSplittedList[repoCounter - 1] + '" could not found, please ensure the the path is actually an git directory')
					i = i + 1
				query.close()
		# endregion
		# region Sync-All

		elif sys.argv[1] == '-Sa' or sys.argv[1] == '--sync-all':
			if len(sys.argv) > 2:
				print('Sync-All does not take any arguments')
			else:
				query = open(queryPath)
				for line in query:
					line = line.replace('~', os.path.expanduser('~'))
					if line.endswith('/'):
						line = line.rstrip('/\n')
					if line.rsplit('/', 1)[-1].rstrip() != '':
						splittedLine = line.rsplit('/', 1)[-1].rstrip()
					else:
						splittedLine = line.rsplit('/', 2)[-2].rstrip()
					try:
						repo = git.Repo(str(line).rstrip())
						origin = repo.remote()
						try:
							with open(line.rstrip() + '/.git/FETCH_HEAD') as head:
								headBeforePull = head.readline()
							origin.pull()
							with open(line.rstrip() + '/.git/FETCH_HEAD') as head:
								headAfterPull = head.readline()
							if headBeforePull == headAfterPull:
								print('The repository "' + splittedLine + '" is already up to date')
							else:
								print('The repository "' + splittedLine + '" is updated')
						except FileNotFoundError:
							print('The head file for the repository "' + splittedLine + '" could not found, please ensure the path is actually an git directory')
					except:
						print('The files for the repository "' + splittedLine + '" could not found, please ensure the the path is actually an git directory')

				query.close()
		# endregion
		# region Invalid-Argument

		else:
			print('You have to enter a valid argument, please type "gitman -H (or) --help" option for the list of valid arguments')
		# endregion

	else:
		print('You have to enter an argument, please type "gitman -H (or) --help" option for the list of valid arguments')
except KeyboardInterrupt:
	print('Program terminated by user key input')
# endregion
