## Table of Contents

[TOC]

## What is Gitman

Gitman, short for Git Manager is a cli program for managing your git packages. Currently only working on Linux, Gitman can be used as a package manager for Git repositories. 

## What can Gitman do

Gitman can manage your repository information in a simple text file and can pull, clone and remove all your repositories at once with a small set of commands.

## What commands are available

`gitman -A (or) --add` for adding existing repositories path to your query.

`gitman -R (or) --remove` for removing repositories inside of your query.

`gitman -Rs (or) --remove-source` for removing repositories from both your query and the actual file path.

`gitman -S (or) --sync` for pulling repositories listed in your query.

`gitman -Sa (or) --sync-all` for pulling all the available repositories in your query.

`gitman -C (or) --clone` for cloning a repository from it's source under the name you give it and add it to your query.

`gitman -Cs (or) --clone-subdir` for cloning a repository to a subdirectory from it's source and add it to your query.

`gitman -Q (or) --query` for listing all the repositories in your query.

`gitman -Qp (or) --query-path` for listing the path of all the repositories in your query.

`gitman -V (or) --version` for showing the version of Gitman.

`gitman -H (or) --help` for showing the help information about Gitman.

## How to install

Run `installer.sh` as the root user ( `sudo ./installer.sh` ), it will copy the `gitman` package to your `/usr/bin` folder.

## The source code

### What language is this

It's Python written like it's C.

### Why is it written like it's C

Because old habits die hard.

### Why the code is this spaghetti

Aside from not using classes it's my first python project and all the python knowledge i have comes from a few hours long online tutorial.

### Then why did you write in Python

Because the only language I'm comfortable in is C#.

### Was this whole section is an excuse for how bad you code

Yes.

## License

Gitman is licensed under [3-Clause BSD License](https://opensource.org/licenses/BSD-3-Clause). You are free to do anything with it.
